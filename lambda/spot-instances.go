package main // gitlab.com/zenyuk_go/go_aws/lambda

import (
	"errors"
	"fmt"
	"math"
	"strconv"
	"time"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
)

// AwsSpotPrice - AWS Spot Instance price with details
type AwsSpotPrice struct {
	AvailabilityZone   string
	InstanceType       string
	ProductDescription string
	SpotPrice          float64
	Timestamp          time.Time
}

// AwsSpotInstanceInterruption - notification that instance will be killed
type AwsSpotInstanceInterruption struct {
	Version    string                            `json:"version"`
	ID         string                            `json:"id"`
	DetailType string                            `json:"detail-type"`
	Source     string                            `json:"source"`
	Account    string                            `json:"account"`
	Time       time.Time                         `json:"time"`
	Region     string                            `json:"region"`
	Resources  []string                          `json:"resources"` // ["arn:aws:ec2:us-east-2:123456789012:instance/i-1234567890abcdef0"]
	Detail     AwsSpotInstanceInterruptionDetail `json:"detail"`
}

// AwsSpotInstanceInterruptionDetail - instance details for AwsSpotInstanceInterruption
type AwsSpotInstanceInterruptionDetail struct {
	InstanceID     string `json:"instance-id"`
	InstanceAction string `json:"instance-action"`
}

// GetCheapestPrice - returns cheapest price for AWS Spot Instances
func GetCheapestPrice(awsSession *session.Session, awsInstanceType *string, awsZone *string) (result AwsSpotPrice, err error) {
	result.SpotPrice = math.MaxFloat64

	ec2Service := ec2.New(awsSession)
	now := time.Now()
	before := now.Add(time.Minute * time.Duration(-30))
	input := &ec2.DescribeSpotPriceHistoryInput{
		EndTime:          &now,
		InstanceTypes:    []*string{awsInstanceType},
		AvailabilityZone: awsZone,
		StartTime:        &before,
	}

	pricesResult, err := ec2Service.DescribeSpotPriceHistory(input)
	if err != nil {
		return result, fmt.Errorf("error getting prices: %+v", err)
	}
	if len(pricesResult.SpotPriceHistory) == 0 {
		return result, errors.New("no prices")
	}

	for _, p := range pricesResult.SpotPriceHistory {
		price, err := strconv.ParseFloat(*p.SpotPrice, 64)
		if err != nil {
			return result, fmt.Errorf("can't parse price: %+v", *p.SpotPrice)
		}
		if price < result.SpotPrice {
			result.SpotPrice = price
			result.AvailabilityZone = *p.AvailabilityZone
			result.InstanceType = *p.InstanceType
			result.ProductDescription = *p.ProductDescription
			result.Timestamp = *p.Timestamp
		}
	}
	return
}

// RequestNewSpotInstance - create a new spot instance
func RequestNewSpotInstance(awsSession *session.Session, awsConfig *AwsConfig, price float64) (newInstanceID string, err error) {
	//aws ec2 request-spot-instances --profile devtest --spot-price "0.13" --launch-specification  file://myLaunchSpecification.json
	ec2Service := ec2.New(awsSession)

	imageID := "ami-5321e331"
	keyName := "AWS-Devtest"
	ebsOptimized := false
	instanceType := "t2.xlarge"
	placementTenancy := "default"
	arn := "arn:aws:iam::389892853624:instance-profile/POC_DomainInstances"

	var networkDeviceIndex int64
	networkSubnetID := "subnet-b886d2dd"
	networkDeleteOnTermination := true
	networkGroup := "sg-4e756a2b"
	networkPublicIP := true

	blockDeviceNameFirst := "/dev/sda1"
	blockDeviceNameSecond := "xvdb"
	blockDeviceEncrypted := false
	blockDeviceFirstDeleteOnTermination := true
	blockDeviceSecondDeleteOnTermination := false
	blockDeviceVolumeType := "gp2"
	var blockDeviceFirstVolumeSize int64 = 50
	var blockDeviceSecondVolumeSize int64 = 100
	blockDeviceFirstSnapshotID := "snap-042c25a2aa8ca73b5"
	blockDeviceSecondSnapshotID := "snap-0f8d0515325f35d48"

	launchSpecification := ec2.RequestSpotLaunchSpecification{
		ImageId:      &imageID,
		KeyName:      &keyName,
		EbsOptimized: &ebsOptimized,
		InstanceType: &instanceType,
		IamInstanceProfile: &ec2.IamInstanceProfileSpecification{
			Arn: &arn,
		},
		NetworkInterfaces: []*ec2.InstanceNetworkInterfaceSpecification{
			&ec2.InstanceNetworkInterfaceSpecification{
				DeviceIndex:              &networkDeviceIndex,
				SubnetId:                 &networkSubnetID,
				DeleteOnTermination:      &networkDeleteOnTermination,
				Groups:                   []*string{&networkGroup},
				AssociatePublicIpAddress: &networkPublicIP,
			},
		},
		Placement: &ec2.SpotPlacement{
			Tenancy:          &placementTenancy,
			AvailabilityZone: &awsConfig.AwsZone,
		},
		BlockDeviceMappings: []*ec2.BlockDeviceMapping{
			&ec2.BlockDeviceMapping{
				DeviceName: &blockDeviceNameFirst,
				Ebs: &ec2.EbsBlockDevice{
					Encrypted:           &blockDeviceEncrypted,
					DeleteOnTermination: &blockDeviceFirstDeleteOnTermination,
					VolumeType:          &blockDeviceVolumeType,
					VolumeSize:          &blockDeviceFirstVolumeSize,
					SnapshotId:          &blockDeviceFirstSnapshotID,
				},
			},
			&ec2.BlockDeviceMapping{
				DeviceName: &blockDeviceNameSecond,
				Ebs: &ec2.EbsBlockDevice{
					Encrypted:           &blockDeviceEncrypted,
					DeleteOnTermination: &blockDeviceSecondDeleteOnTermination,
					VolumeType:          &blockDeviceVolumeType,
					VolumeSize:          &blockDeviceSecondVolumeSize,
					SnapshotId:          &blockDeviceSecondSnapshotID,
				},
			},
		},
	}
	priceString := fmt.Sprintf("%f", price)
	newInstanceRequestOutput, err := ec2Service.RequestSpotInstances(&ec2.RequestSpotInstancesInput{
		LaunchSpecification: &launchSpecification,
		SpotPrice:           &priceString,
	})
	if err != nil || newInstanceRequestOutput == nil || len(newInstanceRequestOutput.SpotInstanceRequests) < 0 {
		return "Error requesting spot instance", err
	}

	var describeOutput *ec2.DescribeSpotInstanceRequestsOutput
	for i := 0; i < 5; i++ {
		time.Sleep(time.Second * 1)
		describeOutput, err = ec2Service.DescribeSpotInstanceRequests(&ec2.DescribeSpotInstanceRequestsInput{
			SpotInstanceRequestIds: []*string{newInstanceRequestOutput.SpotInstanceRequests[0].SpotInstanceRequestId},
		})
		if describeOutput != nil && len(describeOutput.SpotInstanceRequests) > 0 && describeOutput.SpotInstanceRequests[0].InstanceId != nil {
			newInstanceID = *describeOutput.SpotInstanceRequests[0].InstanceId
			return
		}
	}

	return
}
