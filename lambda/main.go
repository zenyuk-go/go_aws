package main // gitlab.com/zenyuk_go/go_aws/lambda

import (
	"context"
	"errors"
	"fmt"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
)

// AwsConfig - basic AWS config
type AwsConfig struct {
	AwsRegion       string
	AwsZone         string
	AwsInstanceType string
}

// MyEvent - payload to the lambda
type MyEvent struct {
	Place string `json:"place"`
}

var awsConfig = AwsConfig{
	"ap-southeast-2",
	"ap-southeast-2" + "a",
	"t2.micro",
}

// HandleRequest - lambda request handler
func HandleRequest(ctx context.Context, event AwsSpotInstanceInterruption) (string, error) {
	if event.Detail.InstanceID == "" {
		return "empty notification, no Instance ID", errors.New("empty notification, no Instance ID")
	}

	awsSession, err := session.NewSession(&aws.Config{
		Region: &awsConfig.AwsRegion},
	)
	if err != nil {
		return fmt.Sprintf("can't create AWS session: %+v", err), err
	}

	// get lowest price
	price, err := GetCheapestPrice(awsSession, &awsConfig.AwsInstanceType, &awsConfig.AwsZone)
	if err != nil {
		return fmt.Sprintf("error getting cheapest price %+v", err), err
	}

	// de-register from load balancer
	err = DeregisterFromLoadbalancer(awsSession, event.Detail.InstanceID)
	if err != nil {
		return "Error removing instance from load balancer", err
	}

	newInstanceID := ""
	for i := 1; i < 5; i++ {
		// request new spot instance incrementally incrising price
		newInstanceID, err = RequestNewSpotInstance(awsSession, &awsConfig, price.SpotPrice+(0.3*float64(i)))
		if err != nil {
			return "Error requesting new spot instances", err
		}
		if newInstanceID != "" {
			break
		}
	}

	if newInstanceID == "" {
		msg := "new instance failed to launch, could be due to too low offering price or hitting instance number limit"
		return msg, errors.New(msg)
	}

	// add to load balancer
	err = RegisterNewInstanceWithLoadbalancer(awsSession, newInstanceID)
	if err != nil {
		return "Error registering instance with load balancer", err
	}

	return fmt.Sprintf("lowest price: %f; instance to be killed: %s; new instance id: %+v\n", price.SpotPrice, event.Detail.InstanceID, newInstanceID), nil
}

func main() {
	lambda.Start(HandleRequest)
}
