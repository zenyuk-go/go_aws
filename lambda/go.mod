module gitlab.com/zenyuk_go/go_aws/lambda

go 1.12

require (
	github.com/aws/aws-lambda-go v1.9.0
	github.com/aws/aws-sdk-go v1.19.2
	github.com/sirupsen/logrus v1.4.0
)
